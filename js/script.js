$(document).ready(function(){

  var speed = 3000;
  var run = setInterval ('rotate()', speed);

  var item_width = $('.quem-gosta__item').outerWidth();
  var left_value = item_width * (-1);

  $('.quem-gosta__item:first').before($('.quem-gosta__item:last'));
  $('.quem-gosta__list').css({'left': left_value});

  $("#prev").click(function(){

    var left_intend = parseInt($('.quem-gosta__list').css('left')) + item_width;
    
    $('.quem-gosta__list').animete({'left':left_intend}, 200, function(){

      $('.quem-gosta__item:first').before($('.quem-gosta__item:last'));
      $('.quem-gosta__list').css({'left': left_value});

    });
  });

  $("#next").click(function(){

    var left_intend = parseInt($('.quem-gosta__list').css('left')) - item_width;
    
    $('.quem-gosta__list').animete({'left':left_intend}, 200, function(){

      $('.quem-gosta__item:last').after($('.quem-gosta__item:first'));
      $('.quem-gosta__list').css({'left': left_value});

    });
  });
});
